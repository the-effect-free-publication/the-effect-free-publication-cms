{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main (main) where

-- import Import

import EffectFree
import EffectFree.Config
import Options.Applicative.Simple
import qualified Paths_the_effect_free_publication_cms
import RIO

parseOpts :: Parser Options
parseOpts =
  Options
    <$> switch
      ( long "verbose"
          <> short 'v'
          <> help "Verbose output?"
      )
    <*> switch
      ( long "auto-migrate"
          <> short 'm'
          <> help "Use auto migration?"
      )

main :: IO ()
main = do
  (options, runCmd) <-
    simpleOptions
      $(simpleVersion Paths_the_effect_free_publication_cms.version)
      "The Org Agile Web Server"
      "Commands for the org agile web server"
      parseOpts
      $ do
        addCommand
          "init"
          "Initialize the database"
          Init
          (pure ())

        addCommand
          "migrate"
          "Perform Migrations"
          Migrate
          (pure ())

        addCommand
          "run"
          "Run Web Server"
          Run
          (pure ())

  config <- readConfig
  runCommand options runCmd config
