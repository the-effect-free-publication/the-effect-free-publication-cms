{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      : EffectFree.Api.Admin
Description : Effect Free Admin API Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Api.Admin (
    AdminAPI,
    adminServer,
    adminApi,
) where

import EffectFree.Api.Admin.Tags (TagsAPI, tagsServer)
import EffectFree.App (AppM)
import Servant

type AdminAPI = "admin" :> TagsAPI

adminServer :: ServerT AdminAPI AppM
adminServer = tagsServer

adminApi :: Proxy AdminAPI
adminApi = Proxy
