{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      : EffectFree.Api.Admin.Tags
Description : Effect Free Admin Tags API Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Api.Admin.Tags (
    TagsAPI,
    tagsServer,
    tagsApi,
) where

import Data.UUID
import RIO
import Servant

import EffectFree.App
import EffectFree.Data.Articles
import EffectFree.Data.Auth

type TagsAPI =
    "tags"
        :> AuthProtect "admin"
        :> QueryParam "page" Int32
        :> QueryParam "perPage" Int32
        :> Get '[JSON] PaginatedTags
        :<|> "tags"
            :> AuthProtect "admin"
            :> Capture "tagId" UUID
            :> Get '[JSON] Tag
        :<|> "tags"
            :> AuthProtect "admin"
            :> Capture "tagSlug" Text
            :> Get '[JSON] Tag

readScope :: User -> AppM ()
readScope = checkScope "read:tags"

writeScope :: User -> AppM ()
writeScope = checkScope "write:tags"

getTags :: User -> Maybe Int32 -> Maybe Int32 -> AppM PaginatedTags
getTags user page perPage = readScope user >> fetchTags "/api/v1/admin/tags" page perPage

getTagById :: User -> UUID -> AppM Tag
getTagById user tagId = readScope user >> fetchTagById tagId

getTagBySlug :: User -> Text -> AppM Tag
getTagBySlug user tagSlug = readScope user >> fetchTagBySlug tagSlug

tagsServer :: ServerT TagsAPI AppM
tagsServer = getTags :<|> getTagById :<|> getTagBySlug

tagsApi :: Proxy TagsAPI
tagsApi = Proxy
