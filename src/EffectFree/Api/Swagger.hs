{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      : EffectFree.Api.Swagger
Description : Org Agile API Documentation Module
License     : AGPL-4.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Api.Swagger (

) where

import Data.OpenApi
import RIO
import Servant
import Servant.Swagger

type SwaggerAPI =
    "swagger.json"
        :> Get '[JSON] OpenApi

apiSpec :: OpenApi
apiSpec =
    info
        . title
        .~ "Effect Free API"
        $ info
        . version
        .~ "0.1.0"
        $ mempty
