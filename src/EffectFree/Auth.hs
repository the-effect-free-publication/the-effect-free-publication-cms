{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      : OrgAgile.Auth
Description : Org Agile Authentication Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Auth (
    AuthCtx,
    fetchJWKS,
    handleAuthentication,
    handleOptionalAuthentication,
) where

import Network.Wai (Request, requestHeaders)
import RIO
import Servant.Server.Experimental.Auth (AuthHandler, mkAuthHandler)

import Control.Monad.Except (runExceptT)
import Control.Monad.Trans.Maybe (MaybeT (..))
import Crypto.JOSE
import Crypto.JWT
import Data.Aeson
import Network.HTTP.Client (httpLbs, newManager, parseRequest, responseBody)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import qualified RIO.ByteString as B
import RIO.ByteString.Lazy (fromStrict)
import qualified RIO.Text as T
import Servant (throwError)

import EffectFree.App
import EffectFree.Data.Auth
import EffectFree.Data.Error (unauthenticatedErrorFmt)

type AuthCtx = AuthHandler Request User ': AuthHandler Request (Maybe User) ': '[]

fetchJWKS :: Text -> IO (Maybe JWKSet)
fetchJWKS url = do
    manager <- newManager tlsManagerSettings
    request <- parseRequest $ T.unpack url
    response <- httpLbs request manager
    return $ decode $ responseBody response

intoMaybe :: Either a b -> Maybe b
intoMaybe (Left _) = Nothing
intoMaybe (Right a) = Just a

verifyToken :: StringOrURI -> JWKSet -> ByteString -> IO (Either JWTError AuthClaims)
verifyToken aud' jwk' authToken = runExceptT $ do
    let config = defaultJWTValidationSettings (== aud')
    jwt <- decodeCompact $ fromStrict authToken
    verifyJWT config jwk' jwt

decodeToken :: StringOrURI -> JWKSet -> ByteString -> MaybeT IO User
decodeToken aud' jwk' authToken = do
    token <- MaybeT . return $ B.stripPrefix "Bearer " authToken
    claims <- MaybeT $ intoMaybe <$> verifyToken aud' jwk' token
    subject <- MaybeT . return $ getSubject claims

    if T.length subject > 0
        then return $ User subject (getScope claims)
        else MaybeT $ return Nothing

getUserFromJwtToken :: Env -> Request -> IO (Maybe User)
getUserFromJwtToken env req = runMaybeT
    $ do
        token <- MaybeT . return $ lookup "Authorization" (requestHeaders req)
        decodeToken (envAud env) (envJWKS env) token

handleAuthentication :: Env -> AuthHandler Request User
handleAuthentication env =
    let handler req = do
            user <- liftIO $ getUserFromJwtToken env req
            case user of
                Nothing -> throwError $ unauthenticatedErrorFmt req
                Just u -> return u
     in mkAuthHandler handler

handleOptionalAuthentication :: Env -> AuthHandler Request (Maybe User)
handleOptionalAuthentication env =
    let handler req = liftIO $ getUserFromJwtToken env req
     in mkAuthHandler handler
