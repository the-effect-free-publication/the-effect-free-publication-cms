{- |
Module      : EffectFree.Db.Schema.PageUtil
Description : Effect Free Database Pagination Utils Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Db.Schema.PageUtil (
  intoLimit,
  intoOffset,
  selectPage,
) where

import Opaleye
import RIO

intoLimit :: Int32 -> Int
intoLimit = fromIntegral

intoOffset :: Int32 -> Int32 -> Int
intoOffset perPage' page' = fromIntegral $ perPage' * (page' - 1)

selectPage :: Select a -> Int32 -> Int32 -> Select a
selectPage query perPage' page' = limit limit' $ offset offset' query
 where
  limit' = intoLimit page'
  offset' = intoOffset page' perPage'
