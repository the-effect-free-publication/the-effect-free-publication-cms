{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}

{- |
Module      : EffectFree.Db.Schema.Tag
Description : Effect Free Database Tag and Categories Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Db.Schema.Tag (
    Tag,
    tagTable,
    selectTags,
    selectTagsPage,
    selectTagById,
    selectTagBySlug,
    Category,
    categoryTable,
    selectCategories,
    selectCategoriesPage,
) where

import Data.Aeson
import Data.Aeson.Casing (aesonPrefix, camelCase)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.UUID (UUID)
import Opaleye
import RIO hiding (Category)

import EffectFree.Db.Schema.PageUtil

data Tag' a b c = Tag
    { tagId :: a
    , tagSlug :: b
    , tagName :: c
    }
    deriving (Eq, Show, Generic)

type Tag = Tag' UUID Text Text
type TagWrite = Tag' (Maybe (Field SqlUuid)) (Field SqlVarcharN) (Field SqlVarcharN)
type TagField = Tag' (Field SqlUuid) (Field SqlVarcharN) (Field SqlVarcharN)

instance FromJSON Tag where
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance ToJSON Tag where
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding = genericToEncoding $ aesonPrefix camelCase

$(makeAdaptorAndInstance "pTag" ''Tag')

tagTable :: Table TagWrite TagField
tagTable =
    table "tags"
        $ pTag
            Tag
                { tagId = tableField "id"
                , tagSlug = tableField "slug"
                , tagName = tableField "name"
                }

data Category' a b c d = Category
    { categoryId :: a
    , categorySlug :: b
    , categoryName :: c
    , categoryDescription :: d
    }
    deriving (Eq, Show, Generic)

type Category = Category' UUID Text Text Text
type CategoryWrite = Category' (Maybe (Field SqlUuid)) (Field SqlVarcharN) (Field SqlVarcharN) (Field SqlText)
type CategoryField = Category' (Field SqlUuid) (Field SqlVarcharN) (Field SqlVarcharN) (Field SqlText)

instance FromJSON Category where
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance ToJSON Category where
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding = genericToEncoding $ aesonPrefix camelCase

$(makeAdaptorAndInstance "pCategory" ''Category')

categoryTable :: Table CategoryWrite CategoryField
categoryTable =
    table "categories"
        $ pCategory
            Category
                { categoryId = tableField "id"
                , categorySlug = tableField "slug"
                , categoryName = tableField "name"
                , categoryDescription = tableField "description"
                }

-- | Queries
selectTags :: Select TagField
selectTags = selectTable tagTable

selectTagsPage :: Int32 -> Int32 -> Select TagField
selectTagsPage = selectPage selectTags

selectTagById :: UUID -> Select TagField
selectTagById tid = do
    tags <- selectTags
    where_ (tagId tags .== toFields tid)

    pure tags

selectTagBySlug :: Text -> Select TagField
selectTagBySlug slug = do
    tags <- selectTags
    where_ (tagSlug tags .== toFields slug)

    pure tags

selectCategories :: Select CategoryField
selectCategories = selectTable categoryTable

selectCategoriesPage :: Int32 -> Int32 -> Select CategoryField
selectCategoriesPage = selectPage selectCategories
