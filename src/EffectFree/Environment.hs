{- |
Module      : OrgAgile.Environment
Description : Org Agile App Environment Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Environment (DbPool, HasDbPool (..), HasJWKSet (..), getDbPool') where

import Crypto.JOSE (JWKSet)
import EffectFree.Db.Pool (DbPool)
import RIO

class HasDbPool env where
    getDbPool :: env -> DbPool

class HasJWKSet env where
    getJWKSet :: env -> JWKSet

getDbPool' :: (HasDbPool env, MonadReader env m) => m DbPool
getDbPool' = ask <&> getDbPool
