{- |
Module      : EffectFree.Data.Articles
Description : Effect Free Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Data.Articles (
    module EffectFree.Db.Schema,
    PaginatedTags,
    fetchTags,
    fetchTagById,
    fetchTagBySlug,
) where

import RIO

import Data.UUID
import Opaleye
import Servant

import EffectFree.App
import EffectFree.Data.Pagination
import EffectFree.Db (runDb, selectQ)
import EffectFree.Db.Schema (Tag)
import qualified EffectFree.Db.Schema as Schema

type PaginatedTags = Pagination Tag

fetchTagsPage :: Maybe Int32 -> Maybe Int32 -> AppM [Tag]
fetchTagsPage page' perPage' = runDb $ selectQ $ Schema.selectTagsPage (defaultPage page') (defaultPerPage perPage')

fetchTagsTotal :: AppM Int64
fetchTagsTotal = do
    total' <- runDb $ selectQ $ countRows Schema.selectTags
    pure $ fromMaybe 0 $ listToMaybe total'

fetchTags :: Text -> Maybe Int32 -> Maybe Int32 -> AppM PaginatedTags
fetchTags uri page' perPage' = do
    tags <- fetchTagsPage page' perPage'
    intoPagination
        tags
        uri
        (defaultPage page')
        (defaultPerPage perPage')
        . fromIntegral
        <$> fetchTagsTotal

fetchTagById :: UUID -> AppM Tag
fetchTagById tagId = do
    tags <- runDb $ selectQ $ Schema.selectTagById tagId
    case tags of
        [] -> throwIO err404
        (tag : _) -> pure tag

fetchTagBySlug :: Text -> AppM Tag
fetchTagBySlug slug = do
    tags <- runDb $ selectQ $ Schema.selectTagBySlug slug
    case tags of
        [] -> throwIO err404
        (tag : _) -> pure tag
