{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE InstanceSigs #-}

{- |
Module      : OrgAgile.Data.Pagination
Description : Org Agile Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Data.Pagination (
    Pagination (..),
    PageLinks (..),
    defaultPage,
    defaultPerPage,
    intoPagination,
) where

import Data.Aeson
import Data.Aeson.Casing
import Data.Aeson.Types (Parser)
import RIO

data PageLinks = PageLinks
    { pageLinksFirst :: Text
    , pageLinksLast :: Text
    , pageLinksNext :: Maybe Text
    , pageLinksPrev :: Maybe Text
    }
    deriving (Show, Eq, Generic)

instance FromJSON PageLinks where
    parseJSON :: Value -> Parser PageLinks
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance ToJSON PageLinks where
    toJSON :: PageLinks -> Value
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding :: PageLinks -> Encoding
    toEncoding = genericToEncoding $ aesonPrefix camelCase

data Pagination a = Pagination
    { paginationData :: [a]
    , paginationPage :: Int32
    , paginationPerPage :: Int32
    , paginationTotalPages :: Int32
    , paginationTotalCount :: Int32
    , paginationLinks :: PageLinks
    }
    deriving (Show, Eq, Generic)

-- instance Functor Pagination where
--    fmap :: (a -> b) -> Pagination a -> Pagination b
--    fmap f (Pagination data' count page perPage) = Pagination (fmap f data') count page perPage

instance (FromJSON a) => FromJSON (Pagination a) where
    parseJSON :: Value -> Parser (Pagination a)
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance (ToJSON a) => ToJSON (Pagination a) where
    toJSON :: Pagination a -> Value
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding :: Pagination a -> Encoding
    toEncoding = genericToEncoding $ aesonPrefix camelCase

defaultPage :: Maybe Int32 -> Int32
defaultPage = fromMaybe 1

defaultPerPage :: Maybe Int32 -> Int32
defaultPerPage = fromMaybe 15

intoPagination :: [a] -> Text -> Int32 -> Int32 -> Int32 -> Pagination a
intoPagination data' uri page perPage count =
    Pagination
        { paginationData = data'
        , paginationPage = page
        , paginationPerPage = perPage
        , paginationTotalPages = totalPages
        , paginationTotalCount = count
        , paginationLinks = pageLinks
        }
  where
    totalPages = div count $ perPage + 1
    pageLinks =
        PageLinks
            { pageLinksFirst = uri <> "?page=1&per_page=" <> tshow perPage
            , pageLinksLast = uri <> "?page=" <> tshow totalPages <> "&per_page=" <> tshow perPage
            , pageLinksNext = if page < totalPages then Just $ uri <> "?page=" <> tshow (page + 1) <> "&per_page=" <> tshow perPage else Nothing
            , pageLinksPrev = if page > 1 then Just $ uri <> "?page=" <> tshow (page - 1) <> "&per_page=" <> tshow perPage else Nothing
            }
