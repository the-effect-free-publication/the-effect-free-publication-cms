{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

{- |
Module      : EffectFree.Data.Auth
Description : The Effect Free Auth Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Data.Auth where

import Crypto.JWT
import Data.Aeson
import Data.Aeson.Casing
import EffectFree.App (AppM)
import RIO
import RIO.Text.Partial (splitOn)
import Servant
import Servant.Server.Experimental.Auth

data AuthClaims = AuthClaims
    { _aClaimSet :: ClaimsSet
    , _aClaimScope :: [Text]
    }
    deriving (Eq, Show, Generic)

claimScope :: Lens' AuthClaims [Text]
claimScope f x = f (_aClaimScope x) <&> \y -> x{_aClaimScope = y}

instance HasClaimsSet AuthClaims where
    claimsSet f s = fmap (\a' -> s{_aClaimSet = a'}) (f $ _aClaimSet s)

instance FromJSON AuthClaims where
    parseJSON = withObject "AuthClaims" $ \o -> do
        _aClaimSet <- parseJSON (Object o)
        _aClaimScope' <- o .: "scope"
        let _aClaimScope = case _aClaimScope' of
                String s -> splitOn " " s
                _ -> []
        return AuthClaims{..}

getScope :: AuthClaims -> [Text]
getScope = _aClaimScope

getSubject :: AuthClaims -> Maybe Text
getSubject claims = fromMaybe "" (claims ^. claimSub) ^? string

data User = User
    { userId :: Text
    , userScopes :: [Text]
    }
    deriving (Eq, Show, Generic)

userIdL :: Lens' User Text
userIdL f x = f (userId x) <&> \y -> x{userId = y}

userScopesL :: Lens' User [Text]
userScopesL f x = f (userScopes x) <&> \y -> x{userScopes = y}

hasScope :: Text -> User -> Bool
hasScope scope user = scope `elem` user ^. userScopesL

checkScope :: Text -> User -> AppM ()
checkScope scope user = do
    unless (hasScope scope user) $ throwIO err403

ensureScope :: Text -> User -> AppM a -> AppM a
ensureScope scope user action = checkScope scope user >> action

instance FromJSON User where
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance ToJSON User where
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding = genericToEncoding $ aesonPrefix camelCase

type instance AuthServerData (AuthProtect "admin") = User

type instance AuthServerData (AuthProtect "optional") = Maybe User
