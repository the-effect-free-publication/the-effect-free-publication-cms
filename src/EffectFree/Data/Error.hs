{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

{- |
Module      : EffectFree.Data.Error
Description : The Effect Free Auth Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree.Data.Error (
    ErrorCode (..),
    ErrorResponse (..),
    errFmtContext,
    serverErrorFmt,
    unauthenticatedErrorFmt,
    unauthorisedErrorFmt,
) where

import Data.Aeson
import qualified Data.Text.Encoding as TE
import Network.Wai (Request, rawPathInfo)
import RIO
import Servant

data ErrorCode
    = NotFound
    | Unauthenticated
    | Unauthorised
    | BadRequest
    | InternalServerError
    deriving (Eq, Show, Generic)

instance ToJSON ErrorCode
instance FromJSON ErrorCode

data ErrorResponse = ErrorResponse
    { errorCode :: ErrorCode
    , errorMessage :: Text
    }
    deriving (Eq, Show)

instance FromJSON ErrorResponse where
    parseJSON = withObject "ErrorResponse" $ \o -> do
        err <- o .: "error"
        errorCode <- err .: "code"
        errorMessage <- err .: "message"
        return ErrorResponse{..}

instance ToJSON ErrorResponse where
    toJSON ErrorResponse{..} =
        object
            [ "error"
                .= object
                    [ "code" .= errorCode
                    , "message" .= errorMessage
                    ]
            ]

    toEncoding ErrorResponse{..} =
        pairs
            ( "error"
                .= object
                    [ "code" .= errorCode
                    , "message" .= errorMessage
                    ]
            )

serverErrorFmt :: ErrorFormatter
serverErrorFmt _ _ _ =
    err500
        { errHeaders = [("Content-Type", "application/json")]
        , errBody = encode $ ErrorResponse InternalServerError "Something went wrong, please try again later."
        }

notFoundErrorFmt :: NotFoundErrorFormatter
notFoundErrorFmt req =
    err404
        { errHeaders = [("Content-Type", "application/json")]
        , errBody = encode $ ErrorResponse NotFound ("The requested resource was not found. At path " <> TE.decodeUtf8 (rawPathInfo req) <> ".")
        }

unauthenticatedErrorFmt :: Request -> ServerError
unauthenticatedErrorFmt req =
    err401
        { errHeaders = [("Content-Type", "application/json")]
        , errBody = encode $ ErrorResponse Unauthenticated $ "You must be authenticated to access resource at path " <> TE.decodeUtf8 (rawPathInfo req) <> "."
        }
unauthorisedErrorFmt :: Request -> ServerError
unauthorisedErrorFmt req =
    err401
        { errHeaders = [("Content-Type", "application/json")]
        , errBody = encode $ ErrorResponse Unauthorised $ "You must be authorized to access resource at path " <> TE.decodeUtf8 (rawPathInfo req) <> "."
        }

errFmtContext :: ErrorFormatters
errFmtContext = defaultErrorFormatters{bodyParserErrorFormatter = serverErrorFmt, urlParseErrorFormatter = serverErrorFmt, notFoundErrorFormatter = notFoundErrorFmt}
