{- |
Module      : EffectFree
Description : The Effect Free Pub Application Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module EffectFree (Command (..), Options (..), runCommand) where

import Data.Pool (withResource)
import EffectFree.Api (api, server)
import EffectFree.App
import EffectFree.Auth
import EffectFree.Config
import EffectFree.Data.Error (errFmtContext)
import EffectFree.Db
import EffectFree.Db.Migration as Migration
import Network.Wai (Middleware)
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.RequestLogger.JSON
import RIO
import Servant

data Command
    = Init ()
    | Migrate ()
    | Run ()

-- | Command line arguments
data Options = Options
    { optionsVerbose :: !Bool
    , optionsAutoMigrate :: !Bool
    }

initDb :: AppM ()
initDb = do
    logInfo "Initializing Database"
    res <- runDb Migration.init
    case res of
        Migration.MigrationSuccess -> logInfo "Database initialized"
        Migration.MigrationError err -> logError $ "Database initialization failed: " <> fromString err

migrateDb :: AppM ()
migrateDb = do
    logInfo "Migrating Database"
    res <- runDb Migration.migrate
    case res of
        Migration.MigrationSuccess -> logInfo "Database migrated"
        Migration.MigrationError err -> logError $ "Database migration failed: " <> fromString err

autoMigrate' :: DbPool -> IO ()
autoMigrate' pool = do
    _ <- withResource pool Migration.autoMigrate
    pure ()

validate' :: DbPool -> IO ()
validate' pool = do
    _ <- withResource pool Migration.validate
    pure ()

mkApp :: Env -> Application
mkApp env =
    serveWithContext api serverAuthContext hoistedServer
  where
    serverAuthContext = errFmtContext :. handleAuthentication env :. handleOptionalAuthentication env :. EmptyContext

    hoistedServer = hoistServerWithContext api (Proxy :: Proxy AuthCtx) (convertApp env) server

jsonRequestLogger :: IO Middleware
jsonRequestLogger =
    mkRequestLogger
        $ defaultRequestLoggerSettings
            { outputFormat = CustomOutputFormatWithDetails formatAsJSON
            }

runApp :: Int -> Env -> IO ()
runApp port env = do
    warpLogger <- jsonRequestLogger
    let warpSettings =
            Warp.defaultSettings
                & Warp.setPort port
                & Warp.setTimeout 60
    Warp.runSettings warpSettings $ warpLogger $ mkApp env

runCommand :: Options -> Command -> Config -> IO ()
runCommand opts cmd cfg = do
    pool <- loadPool $ configDbConfig cfg

    lo' <- logOptionsHandle stderr (optionsVerbose opts)
    let lo = setLogMinLevel (vLogLevel . configAppLevel $ cfg) lo'

    resp <- fetchJWKS $ configJwksUrl cfg
    jwk <- case resp of
        Nothing -> error "Failed to fetch JWKS"
        Just jwk' -> pure jwk'

    withLogFunc lo $ \lf ->
        let env =
                Env
                    { envDbPool = pool
                    , envLogFunc = lf
                    , envJWKS = jwk
                    , envAud = configAudience cfg
                    }
         in case cmd of
                Init _ -> runRIO env initDb
                Migrate _ -> runRIO env migrateDb
                Run _ -> do
                    if optionsAutoMigrate opts
                        then autoMigrate' pool
                        else validate' pool
                    runApp (configPort cfg) env
