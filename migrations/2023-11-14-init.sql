CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE contributors (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    slug varchar (31) UNIQUE NOT NULL,
    name varchar (255) NOT NULL,
    profile_image varchar (255) NOT NULL,
    cover_image varchar (255) NOT NULL,
    bio text NOT NULL,
    location varchar (255),
    website varchar (255),
    linkedin varchar (255),
    instagram varchar (255),
    facebook varchar (255),
    twitter varchar (255)
);

CREATE TABLE tags (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name varchar (63) NOT NULL
);

CREATE TABLE categories (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    slug varchar (31) UNIQUE NOT NULL,
    name varchar (63) NOT NULL,
    description text NOT NULL
);

INSERT INTO categories (slug, name, description)
VALUES ('editorial', 'Editorial', 'Editorial Articles and Pieces');

CREATE TABLE publications (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  slug varchar(63) NOT NULL,
  title varchar(255) NOT NULL,
  issue int NOT NULL,
  published boolean NOT NULL,
  published_at timestamp,
  UNIQUE (slug, issue)
);

INSERT INTO publications (slug, title, issue, published)
VALUES ('online', 'Online Exclusive', '0', TRUE);

CREATE TABLE publications_editors (
    publication_id uuid REFERENCES publications (id),
    editor_id uuid REFERENCES contributors (id),
    PRIMARY KEY (publication_id, editor_id)
);

CREATE TYPE t_visibility AS ENUM ('draft','public','subscriber');

CREATE TABLE articles (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    slug varchar (63) UNIQUE NOT NULL,
    title varchar (255) NOT NULL,
    excerpt text,
    html text NOT NULL,
    publication_id uuid NOT NULL REFERENCES publications (id),
    category_id uuid NOT NULL REFERENCES categories (id),
    visibility t_visibility NOT NULL DEFAULT 'draft',
    published_at timestamp,
    created_at timestamp NOT NULL DEFAULT now(),
    updated_at timestamp NOT NULL DEFAULT now()
);

CREATE TABLE feature_image (
  id uuid PRIMARY KEY REFERENCES articles (id),
  image varchar (255) NOT NULL,
  alt varchar (255),
  caption varchar(255)
);

CREATE TABLE articles_tags (
    article_id uuid REFERENCES articles (id),
    tag_id uuid REFERENCES tags (id),
    PRIMARY KEY (article_id, tag_id)
);

CREATE TABLE comments (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    article_id uuid NOT NULL REFERENCES articles (id),
    username varchar (255) NOT NULL,
    content text NOT NULL,
    created_at timestamp NOT NULL DEFAULT now(),
    updated_at timestamp NOT NULL DEFAULT now()
);
