-- Create a function to generate slugs
CREATE OR REPLACE FUNCTION generate_slug(input_text VARCHAR)
    RETURNS VARCHAR AS $$
DECLARE
    result_text VARCHAR;
BEGIN
    -- Replace spaces with dashes
    result_text := REPLACE(input_text, ' ', '-');

    -- Remove non-alphabetic characters
    result_text := REGEXP_REPLACE(result_text, '[^a-zA-Z0-9-]', '', 'g');

    -- Lowercase the result (optional, you may choose to keep it uppercase)
    result_text := LOWER(result_text);

    RETURN result_text;
END;
$$ LANGUAGE plpgsql;

-- Add a new column 'slug' to the 'blog_tag' table
ALTER TABLE tags
    ADD COLUMN slug VARCHAR(31);

-- Update the 'slug' column using the generate_slug function
UPDATE tags SET slug = generate_slug(name);

-- Add a unique constraint to the 'slug' column
ALTER TABLE tags ADD CONSTRAINT tags_slug_key UNIQUE (slug);
